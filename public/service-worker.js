// service-worker.js
const cacheVersion = 1; // Increment this number when you update your service worker.
const cacheName = `app-cache-v${cacheVersion}`;
const cacheFirstFiles = ['/pouchdb.min.js'];
const networkFirstFiles = ['/', '/index.html','/logo.png'];

console.log('Service Worker: Hello World!');

self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(cacheName).then(cache => {
            return Promise.all(cacheFirstFiles.concat(networkFirstFiles).map(url =>
                fetch(url).then(response => {
                    if (!response.ok) throw new Error(`Request failed: ${url}`);
                    return cache.put(url, response);
                })
            ));
        })
    );
});

self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(name => {
                    if (name !== cacheName) {
                        return caches.delete(name);
                    }
                })
            );
        })
    );
});

self.addEventListener('fetch', event => {
    const request = event.request;
    const url = new URL(request.url);

    if (url.pathname === '/pouchdb.min.js') {
        event.respondWith(cacheFirst(request));
        return;
    }

    if (event.request.mode === 'navigate' || networkFirstFiles.includes(url.pathname) || /^\/_nuxt/.test(url.pathname) || url.pathname.endsWith('.js')) {
        event.respondWith(networkFirst(request));
        return;
    }

    event.respondWith(cacheFirst(request));
});

async function cacheFirst(request) {
    const cachedResponse = await caches.match(request);
    return cachedResponse || fetch(request);
}

async function networkFirst(request) {
    const cache = await caches.open(cacheName);

    try {
        const response = await fetch(request);
        cache.put(request, response.clone());
        return response;
    } catch (error) {
        const cachedResponse = await cache.match(request);
        return cachedResponse || error;
    }
}

self.addEventListener('offline', () => {
    console.log('App is offline');
});

self.addEventListener('online', () => {
    console.log('App is online');
});
