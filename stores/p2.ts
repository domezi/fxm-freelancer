import {defineStore} from 'pinia'
const db = new PouchDB('dbname')

export const useP2Store = defineStore('auth', {
    state: () => ({
        hours: [],
        latestLocalSeq: null,
        latestSyncedSeq: localStorage.getItem('latestSyncedSeq') || null,
    }),
    actions: {
        addHour(minutes: number, description: string) {
            const doc = {
                _id: 'hour_freelance/' + new Date().toISOString() + '/' + Math.random().toString(36).substring(7),
                minutes,
                description
            }
            db.put(doc)
            this.hours.unshift(doc)
        }
    }
})

// initially load hours from pouchdb
const refresh = () => {
    // get last seq
    db.info().then((info) => {
        console.log("latest seq", info.update_seq)
        useP2Store().latestSeq = info.update_seq
    })
    db.allDocs({
        include_docs: true,
        endkey: 'hour_freelance',
        startkey: 'hour_freelance\uffff',
        descending: true
    }).then((res) => {
        console.log(res)
        useP2Store().hours = res.rows.map((row) => row.doc)
        // return res.rows.map((row) => row.doc)
    })
}

refresh()

const sync = () => {
    // go to server: "Hey server, I need to inform you about what happened since latestSyncedSeq"

    // TODO: send changes since latestSyncedSeq to server

    // server goes: "Thanks. Now first of all here's what happened since
    // latestSyncedSeq from my side and now I am adding your changes to my side.
    // Your latestSyncedSeq is now the same as my latestSyncedSeq."

    // TODO: receive changes from server and apply them to local db, then update latestSyncedSeq

}