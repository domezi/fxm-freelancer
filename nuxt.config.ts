// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    app: {
        title: 'tilel wton',
        head: {
            title: 'Freelancer',
            //icon.png
            meta: [
                {charset: 'utf-8'},
                {name: 'viewport', content: 'width=device-width, initial-scale=1'},
                {hid: 'description', name: 'description', content: 'Freelancer'},
                {name: 'format-detection', content: 'telephone=no'},
                {name: 'apple-mobile-web-app-title', content: 'Freelancer'},
                {name: 'apple-mobile-web-app-capable', content: 'yes'},
                {name: 'apple-mobile-web-app-status-bar-style', content: 'black-translucent'},
            ],
            link: [
                {rel: 'manifest', href: '/manifest.json'},
                {rel: 'icon', type: 'image/png', href: '/icon.png'},
                {rel: 'apple-touch-icon', href: '/icon.png'},

            ],
            script: [
                {
                    src: '/pouchdb.min.js',
                },
                {
                    src: '/register-service-worker.js',
                }
            ]
        }
    },
    modules: [
        '@pinia/nuxt',
        '@invictus.codes/nuxt-vuetify'
    ],
    ssr: false,


})
